# EasyPopUp [![](http://img.shields.io/badge/Swift-1.2-blue.svg)]() [![](http://img.shields.io/badge/iOS-7.0%2B-blue.svg)]() [![](http://img.shields.io/badge/iOS-8.0%2B-blue.svg)]() [![](http://img.shields.io/badge/iOS-9.0%2B-blue.svg)]() 
Easy popup that can be used in Swift projects. EasyPopUp is a subclass of UIView and expects a UIView in its initializer.

<p align="center">
        <img src="http://zippy.gfycat.com/FirmFrankCurlew.gif" />
</p>


## Contributions & Share
* Any contribution is more than welcome! You can contribute through pull requests and issues on GitHub. :D
* Send me your application's link, if you use Easy Game center, I will add on the cover page and for support [@RedWolfStudioFR](https://twitter.com/RedWolfStudioFR) 
[@YannickSteph](https://twitter.com/YannickSteph)

## Support
* Contact for support [Issues](https://github.com/DaRkD0G/Easy-Game-Center-Swift/issues)
* [@RedWolfStudioFR](https://twitter.com/RedWolfStudioFR) // [@YannickSteph](https://twitter.com/YannickSteph)

# Documentation
## Setup
**1.** Add the following classes (EasyPopUp.swift) to your Xcode project (make sure to select Copy Items in the dialog)

**2.** Create PopupView in your UIViewController : 
```swift
  /// Create popup
  let popupView = EasyPopUp(frame: self.view.frame, contentView: contentView)
  /// Add to subview
  self.view.addSubview(popupView)
  /// Open Popup
  popupView.start()
```
        
#Parameter
###Popup View
* **Description :** Popview it's your popup window
```swift
        // UIView
        popupView.popupView
```
###Popup Close button
* **Description :** closeButton it's your button close in your popup window
```swift
        // UIButton
        popupView.closeButton
```
###Popup background 
* **Description :** closeButton it's your background behind your popup window
```swift
        // UIView
        popupView.backgroundView
```
###Set size button close
* **Description :** Set size for button close with and height 
```swift
        popupView.sizeCloseBtnPourcent = 20
```
###Set height by pourcent of Popup
* **Description :** Set height by pourcent of size of screen
```swift
        /// 80% of height of screen 
        popupView.heightPopPourcent = 80
```
###Set width by pourcent of Popup
* **Description :** Set width by pourcent of size of screen
```swift
        /// 70% of width of screen 
        popupView.widthPopPourcent = 70
```
###Set offset at top
* **Description :** Set offset at top, it's size not pourcent
```swift
        /// 70% of width of screen 
        popupView.offsetYMore = 50
```
###Popup startAnimate 
* **Description :** This is where you can customize the animation to enter
* If you not set, the default animation takes
```swift
        // Example
        popupView.startAnimate = {
            // Custom Background
            popupView.backgroundView.alpha = 0
            popupView.backgroundView.backgroundColor = UIColor.redColor()
            
            // Custom PopupView
            popupView.popupView.frame.origin.x = -popupView.frame.width
            popupView.popupView.frame.origin.y = -popupView.frame.height
            
            
            // Custom Close Button
            popupView.closeButton.frame.origin.x = -popupView.frame.width
            popupView.closeButton.frame.origin.y = -popupView.frame.height
            
            // Add animation
            UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                popupView.backgroundView.alpha = 0.8
                popupView.popupView.frame.origin = CGPointMake(popupView.offsetX, popupView.offsetY)
                
                let offSetBtnX = popupView.offsetX - popupView.sizeButtonClose / 2
                let offSetBtnY = popupView.offsetY - popupView.sizeButtonClose / 2
                
                popupView.closeButton.frame.origin = CGPointMake(offSetBtnX, offSetBtnY)
                
                }, completion: nil)
        }
```
###Popup startAnimate 
* **Description :** This is where you can customize the animation to end
* If you not set, the default animation takes, remember to remove popview with removeFromSuperview
```swift
        // Example
        popupView.endAnimate = {
            
            UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                
                popupView.backgroundView.alpha = 0
                
                popupView.popupView.frame.origin = CGPointMake(popupView.frame.width, popupView.frame.height)
                popupView.closeButton.frame.origin = CGPointMake(popupView.frame.width, popupView.frame.height)
                
                
                
                }) { (Bool) -> Void in
                        
                    popupView.removeFromSuperview()
            }
        }
```

### Legacy support
For support of iOS 7+ & iOS 8+ & iOS 9+ 

[@RedWolfStudioFR](https://twitter.com/RedWolfStudioFR) 

[@YannickSteph](https://twitter.com/YannickSteph)

Yannick Stephan works hard to have as high feature parity with **Easy Game Center** as possible. 

### License
The MIT License (MIT)

Copyright (c) 2015 Red Wolf Studio, Yannick Stephan

[Red Wolf Studio](http://www.redwolfstudio.fr)

[Yannick Stephan](http://yannickstephan.com)
