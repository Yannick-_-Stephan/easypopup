//
//  ViewController.swift
//  EasyPopup
//
//  Created by DaRk-_-D0G on 24/08/2015.
//  Copyright (c) 2015 DaRk-_-D0G. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    lazy var myContentPopUpDemoDefault:UIView = {
        let contentView = self.defaultContentView(self.view.frame.size.width,height: self.view.frame.size.height,imageName:"peace")
        return contentView
        }()
    
    ///  Firt Label
    lazy var myContentPopUpDemo1:UIView = {
        let contentView = self.defaultContentView(self.view.frame.size.width,height: self.view.frame.size.height,imageName:"peace2")
        return contentView
        }()
    
    lazy var myContentPopUpDemo2:UIView = {
        let contentView = self.defaultContentView(self.view.frame.size.width,height: self.view.frame.size.height,imageName:"peace3")
        return contentView
        }()
    
    lazy var myContentPopUpDemo3:UIView = {
        let contentView = self.defaultContentView(self.view.frame.size.width,height: self.view.frame.size.height,imageName:"", offSet: -210)
        return contentView
        }()
    
    
    /**
    Open demo default
    
    
    */
    @IBAction func ActionOpenPopUpDefault(sender: AnyObject) {
        let popupView =  EasyPopup(frame:self.view.frame, contentView: myContentPopUpDemoDefault)
        
        
        self.view.addSubview(popupView)
        
        //Start open popup
        popupView.start()
    }
    
    /**
    Open demo 1
    
    
    */
    @IBAction func ActionOpenPopUpDemo1(sender: AnyObject) {
        
        let popupView =  EasyPopup(frame:self.view.frame, contentView: myContentPopUpDemo1)
        
        // Custom height width by pourcent ( 100 % = height or width of Screen)
        popupView.heightPopPourcent = 80
        popupView.widthPopPourcent = 80
        popupView.positionYMore = 30
        
        
        
        /**
        *  Custom Start animate
        */
        popupView.startAnimate = {
            
            popupView.backgroundView.alpha = 0
            popupView.backgroundView.backgroundColor = UIColor.blueColor()
            
            popupView.popupView.frame.origin.x = -popupView.frame.width
            popupView.closeButton.frame.origin.x = -popupView.frame.width
            
            UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                popupView.backgroundView.alpha = 0.3
                
                popupView.popupView.frame.origin.x = popupView.positionX
                popupView.closeButton.frame.origin.x  = popupView.positionX - popupView.sizeButtonClose / 2
                
                }, completion: nil)
        }
        /**
        *  Custom End animate
        */
        popupView.endAnimate = {
            
            UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                popupView.backgroundView.alpha = 0
                popupView.popupView.frame.origin.x = popupView.frame.width
                popupView.closeButton.frame.origin.x  = popupView.frame.width
                
                
                }) { (Bool) -> Void in
                    popupView.removeFromSuperview()
            }
        }
        
        popupView.start()
        
        self.view.addSubview(popupView)
    }
    
    /**
    Open demo 2
    
    
    */
    @IBAction func ActionOpenPopUpDemo2(sender: AnyObject) {
        
        let popupView =  EasyPopup(frame:self.view.frame, contentView: myContentPopUpDemo2)
        
        popupView.heightPopPourcent = 70
        popupView.widthPopPourcent = 80
        popupView.positionYMore = 10
        
        /**
        *  Custom Start animate
        */
        popupView.startAnimate = {
            
            popupView.backgroundView.alpha = 0
            popupView.backgroundView.backgroundColor = UIColor.redColor()
            
            popupView.popupView.frame.origin.x = -popupView.frame.width
            popupView.closeButton.frame.origin.x = -popupView.frame.width
            
            popupView.popupView.frame.origin.y = -popupView.frame.height
            popupView.closeButton.frame.origin.y = -popupView.frame.height
            
            
            UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                popupView.backgroundView.alpha = 0.8
                popupView.popupView.frame.origin = CGPointMake(popupView.positionX, popupView.positionY)
                
                let offSetBtnX = popupView.positionX - popupView.sizeButtonClose / 2
                let offSetBtnY = popupView.positionY - popupView.sizeButtonClose / 2
                
                popupView.closeButton.frame.origin = CGPointMake(offSetBtnX, offSetBtnY)
                
                }, completion: nil)
        }
        /**
        *  Custom End animate
        */
        popupView.endAnimate = {
            
            UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                
                popupView.backgroundView.alpha = 0
                
                popupView.popupView.frame.origin = CGPointMake(popupView.frame.width, popupView.frame.height)
                popupView.closeButton.frame.origin = CGPointMake(popupView.frame.width, popupView.frame.height)
                
                
                
                }) { (Bool) -> Void in
                    popupView.removeFromSuperview()
            }
        }
        
        popupView.start()
        
        self.view.addSubview(popupView)
    }
    
    
    /**
    Open demo 3
    
    
    */
    @IBAction func ActionOpenPopUpDemo3(sender: AnyObject) {
        
        let popupView =  EasyPopup(frame:self.view.frame, contentView: myContentPopUpDemo3)
        
        
        popupView.heightPopPourcent = 10
        popupView.widthPopPourcent = 80
        popupView.positionYMore = 10
        popupView.backgroundView.backgroundColor = UIColor.orangeColor()
        /**
        *  Custom Start animate
        */
        // default animate
        /**
        *  Custom End animate
        */
        popupView.endAnimate = {
            UIView.animateWithDuration(0.4, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                popupView.backgroundView.alpha = 0
                popupView.popupView.frame.origin.y = -popupView.frame.height
                popupView.closeButton.frame.origin.y  = -popupView.frame.height
                
                
                }) { (Bool) -> Void in
                    popupView.removeFromSuperview()
            }
        }
        
        popupView.start()
        
        self.view.addSubview(popupView)
    }
    
    /**
    Create image by name
    
    :param: contentView UIView
    :param: name        String
    */
    func defaultImage(contentView:UIView,name:String) {
        if name != "" {
            let image = UIImage(named: name)
            let imageView = UIImageView(image: image)
            contentView.addSubview(imageView)
            
            
            
            imageView.autoresizingMask = UIViewAutoresizing.FlexibleLeftMargin |
                UIViewAutoresizing.FlexibleRightMargin |
                UIViewAutoresizing.FlexibleTopMargin |
                UIViewAutoresizing.FlexibleBottomMargin
            imageView.center = CGPointMake(view.bounds.midX, view.bounds.midY + imageView.frame.height - 200)
        }
        
    }
    /**
    Create content view
    
    :param: width     CGFloat
    :param: height    CGFloat
    :param: imageName String
    
    :returns: UIView
    */
    func defaultContentView(width:CGFloat,height:CGFloat, imageName:String, offSet:CGFloat = -200) -> UIView {
        var contentView = UIView(frame: CGRectMake(0, 0, width, height ))
        
        
        // create the title label
        let titleLabel = UILabel(frame: CGRectMake(0, 0, width, 30))
        titleLabel.text = "Example blabla, Cool popup !"
        titleLabel.textAlignment = .Center
        titleLabel.font = UIFont.systemFontOfSize(17)
        contentView.addSubview(titleLabel)
        
        contentView.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        
        titleLabel.autoresizingMask = UIViewAutoresizing.FlexibleLeftMargin |
            UIViewAutoresizing.FlexibleRightMargin |
            UIViewAutoresizing.FlexibleTopMargin |
            UIViewAutoresizing.FlexibleBottomMargin
        titleLabel.center = CGPointMake(view.bounds.midX, view.bounds.midY + offSet)
        self.defaultImage(contentView,name: imageName)
        
        return contentView
    }
    
}

